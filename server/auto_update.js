var app = require('../server.js');

var dataSource = app.dataSources['ektix_rules_mongodb'];

dataSource.on('connected', function() {
  dataSource.autoupdate(function(err) {
    if (err) {
      console.error('Database could not be autoupdated', err);
      dataSource.disconnect();
      return;
    }
    console.log('Database autoupdated');
    dataSource.disconnect();
  });
}); 
