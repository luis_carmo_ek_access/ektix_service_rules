'use strict';

module.exports = function(Ektixattributenames) {

	Ektixattributenames.get_byExtID = function(external_id, cb) {

	Ektixattributenames.findOne({where: {external_id: external_id}}, function(err, results) {
			if (err){ 
				return cb(null, []);
				throw err;
			}

			if(results == null){
				return cb(null, []);
			}else{
				var response = {
					"external_id": results.external_id,
					"attribute_group_name_id": results.attribute_group_name_id,
					"tag": results.tag,
					"sort_order": results.sort_order,

				}

				return cb(null, response);
			}
		});
	}



	Ektixattributenames.remoteMethod('get_byExtID', {
		description:"",
		http:{
			path:'/get_byExtID',
			verb : 'get',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'external_id', type : 'number', required: true},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

};
