'use strict';

module.exports = function(Ektixvalidationrequest) {

	var _ = require('underscore');
	var Moment = require('moment-timezone');
	var MongoClient = require('mongodb').MongoClient;
	var url = "mongodb://localhost:8015/ektix_rules";
	var now = require("performance-now")


	Ektixvalidationrequest.rule_validation = function(request_rule_id, limit, skip, identification, cb) {
		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;

		var validations = [];
		
		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";

  		var array_insert = [];
		array_insert.push(request_rule_id);

		var obj_request = {
			"request_rule_id": array_insert,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		console.log("Request: ", request_rule_id);
      		request_id = inserted.id;
      		console.log("request_id: ", request_id);
      	
      		///////////////////////////////////////////
			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
				//validations = validations_relational;

				console.log("validations_relational", validations_relational);

				if(validations_relational.length > 0){ 
					get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
						var begin = 0;

						calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, function(final_result){

							date = Moment().utc("Europe/Lisbon").format();
							

							var obj_response = {
								"request_id": request_id,
							    "response": final_result.length,
							    "created_date": date,
							    "modified_date": date
							}

							validation_response.create(obj_response, function(err, inserted) {
					      		if (err) throw err;

					      		var response_id = inserted.id;
      							console.log("response_id: ", response_id);

							});	

							return cb(null, final_result);					
						});
					});
				}else{
					return cb(null, []);
				}
			});
			//////////////////////////////////////////////
		});
	}

	Ektixvalidationrequest.multiRule_validation = function(request_array_rules_id, limit, skip, identification, cb) {
		var start = now()

		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;


		var array_validations = []

		console.log("request_array_rules_id", request_array_rules_id);

		var request_size = request_array_rules_id.length;
		var count = 1;
		var querys = [];

		//console.log("request_size", request_size);

		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";
		var obj_request = {
			"request_rule_id": request_array_rules_id,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		request_id = inserted.id;
      		console.log("request_id: ", request_id);

      		if(request_size == 1){ // Se for apenas uma regra
				//console.log("IIIIIIIIIIIIFFFFFFFFFFFFFFFFFFFFFFFFFFf");
				var request_rule_id = request_array_rules_id[0];

				/////////////////////////
				get_rule_info(request_rule_id, rules, function(rule_info){
					//console.log("rule_info", rule_info);
	      			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
	      				//console.log("validations_relational", validations_relational);
						if(validations_relational.length > 0){ 
							get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
								var begin = 0;

								calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, identification, function(final_result){
									date = Moment().utc("Europe/Lisbon").format();

									var obj_response = {
										"request_id": request_id,
									    "response": final_result.length,
									    "created_date": date,
									    "modified_date": date
									}


								console.log('**********************');
								console.log(final_result);

									validation_response.create(obj_response, function(err, inserted) {
							      		if (err) throw err;

							      		var response_id = inserted.id;
		      							console.log("response_id: ", response_id);
		      							
		      							var end = now();
										console.log("Tempo processamento: ", (end-start).toFixed(3));
		      							
		      							console.log("!########################!");
									});	
									return cb(null, final_result);					
								});
							});
						}else{
							return cb(null, []);
						}
					});
      			});
				//////////////////
      		}else{ // se forem várias regras
      			console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEELLLLLLLLLLLLLL");
				request_array_rules_id.forEach(function(request_rule_id) {
					//console.log("rules_id", request_rule_id);

					get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
						get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
							build_querys(validations_relational, validations_logical, function(querys_result){

								//console.log(count);
								//console.log(querys_result);

								if(querys_result.length > 0){
									//console.log("TTTTTTTTTTTTTTT");
									querys.push(querys_result);
								}

								if(count == request_size){
									//console.log("querys", querys);
									if(querys.length > 0){
										execute_querys(querys, limit, skip, attribute_values, identification, function(final_result){

											date = Moment().utc("Europe/Lisbon").format();
											var obj_response = {
												"request_id": request_id,
											    "response": final_result.length,
											    "created_date": date,
											    "modified_date": date
											}

											validation_response.create(obj_response, function(err, inserted) {
									      		if (err) throw err;

									      		var response_id = inserted.id;
				      							console.log("response_id: ", response_id);

				      							var end = now();
												console.log("Tempo processamento: ", (end-start).toFixed(3));
		      							
		      									console.log("!########################!");

											});	
											return cb(null, final_result);
										});
									} else{
										return cb(null, querys);
									}
								}
								count++;
							});
						});
					});
				});
			}
		});
		//console.log(querys);
	}


	function execute_querys(querys, limit, skip, attribute_values, identification, cb){
		console.log(execute_querys);
		//console.log(querys);

		var count = 1;
		var querys_size =  querys.length;

		var result = "";
		var close = "]}";
		var and_open = 0;

		if(querys.length == 1){
			result = querys[0];
			//console.log("11111111111111111111111111111111111", result);
		}else{
			querys.forEach(function(q) {
				//console.log(q);
				//console.log(result); 

				if(and_open == 0){
					//console.log("IIIIIIIIIIIIIIIIIIIIIIIIIII");

					if(result){
						//result = '{"or":[' + q + ", " + result + close;
						result = '{"$or":[' + q + ", " + result + close;
					}else{
						//result = '{"or":[' + q + ", ";
						result = '{"$or":[' + q + ", ";
						and_open = 1;	
					}
				}else{
					//console.log("EEEEEEEEEEEEEEEEEEEEEEEEEE");
					if(and_open == 1){
						//console.log("IIIIII2222222222222222");
						result = result + q + close;
						and_open = 0;
					}
				}

				//console.log("------------------", result);
				count++;
			});
		}
		
		var limit_string = "";
		var skip_string = "";
		var identification_string = "";


		var limit_mongo = 0;
		var skip_mongo = 0;
		var identification_mongo = "";

		if(typeof limit  !== 'undefined'){
			limit_string = '"limit":"' + String(limit) + '",';
			//console.log(limit_string);
			limit_mongo = parseInt(limit);
		}

		if(typeof skip  !== 'undefined'){
			skip_string = '"skip":"' + String(skip) + '",';
			//console.log(skip_string);
			skip_mongo = parseInt(skip);
		}

		//console.log("identification", identification);


		if(typeof identification  !== 'undefined'){
			Object.keys(identification).forEach(function (k) {
				identification_string += '{"identification.'+ k +'":"'+ identification[k] +'"},';
				identification_mongo +=  '{"identification.'+ k +'":"'+ identification[k] +'"},';
			});

			result = '{"and":['+ identification_string  + result + ']}';
			query_mongo = '{"$and":['+ identification_mongo  + query_mongo + ']}';
		}

		console.log('{"where":'+ result +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');		

		var final_query = JSON.parse('{"where":'+ result +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');

		//attribute_values.find( final_query , function(err, results_query){
		//	if(err) throw err;

		//	cb(results_query);

		//});

		console.log("---------------------------------V2", result, typeof result);

		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;

		  //var query_mongo = { address: /^S/ };

		  db.collection("ektix_attribute_values").find(JSON.parse(result)).sort({attribute_group_value_id: 1}).limit(limit_mongo).skip(skip_mongo).toArray(function(err, result) {
		    if (err) throw err;
		    //console.log(result);
		    db.close();
		    cb(result);
		  });
		});
	}

	function build_querys(validations_relational, validations_logical, cb){
		console.log(build_querys);
		var stack = [];

		console.log("RRRRRRRRRRRRRRRRRRRR",validations_relational);
		console.log("LLLLLLLLLLLLLLLLLLLL",validations_logical);

		Array.prototype.push.apply(stack,validations_relational);
		Array.prototype.push.apply(stack,validations_logical);

		stack.sort(function(a, b) {
    		return a.post_order - b.post_order;
		});

		//console.log(stack);

		var query = "";
		var close = "";

		//query = recursive_build(stack);

		if(validations_relational.length != 0 || validations_logical.length != 0){
			query = recursive_build_mongo(stack);	
		}

		console.log("/////////////////", query);
		cb(query);
	}


	function calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, identification, cb){
		console.log(calculate_expression_V2);

		var stack = [];

		Array.prototype.push.apply(stack,validations_relational);
		Array.prototype.push.apply(stack,validations_logical);

		stack.sort(function(a, b) {
			return a.post_order - b.post_order;
    		//return b.post_order - a.post_order;
		});

		//console.log(stack);

		var query = "";
		var close = "";

		var operator_before = 1;

		//query = recursive_build(stack);

		//console.log(recursive_build_mongo(stack));
		console.log(1);

		var query_mongo = recursive_build_mongo(stack);
		
		console.log("---------------------------------", query_mongo, typeof query_mongo);
		console.log(query_mongo);

		//var search_obj = JSON.stringify(query_mongo);
		//console.log("---------------------------------", search_obj, typeof search_obj);
		

		var limit_string = "";
		var skip_string = "";
		var identification_string = "";

		var limit_mongo = 0;
		var skip_mongo = 0;
		var identification_mongo = "";

		if(typeof limit  !== 'undefined'){
			limit_string = '"limit":"' + String(limit) + '",';
			//console.log(limit_string);
			limit_mongo = parseInt(limit);
		}


		if(typeof skip  !== 'undefined'){
			skip_string = '"skip":"' + String(skip) + '",';
			//console.log(skip_string);
			skip_mongo = parseInt(skip);
		}

		//console.log("identification", identification, typeof identification);


		if(typeof identification  !== 'undefined'){
			Object.keys(identification).forEach(function (k) {
				
				if (k == 'seat_id') {
					identification_mongo +=  '{"identification.'+ k +'":'+ identification[k] +'},';
				} else {
					identification_mongo +=  '{"identification.'+ k +'":"'+ identification[k] +'"},';
				}

			});

			query_mongo = '{"$and":['+ identification_mongo  + query_mongo + ']}';

		}


		//console.log("---------------------------------V2", query_mongo, typeof query_mongo);

		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;

		  //var query_mongo = { address: /^S/ };

		  db.collection("ektix_attribute_values").find(JSON.parse(query_mongo)).sort({"attribute_group_value_id": 1}).limit(limit_mongo).skip(skip_mongo).toArray(function(err, result) {
		    if (err) throw err;
		    db.close();
		    cb(result);
		  });
		});
	}


	function recursive_build(stack) {
        console.log(recursive_build);
        console.log(stack);

        if (stack.length == 1) {
            return stack[0]["search_pattern"];
        } else {
            //console.log(stack.length == 2);
            //console.log(stack[0].hasOwnProperty('operator'));
            //if (stack.length == 2 && stack[0].hasOwnProperty('operator')) {
            if (stack.length == 2 && ('operator' in stack[0])) {
                return stack[1]["search_pattern"];
            } else {
                var s = stack[0];
                stack.shift();
                //if (s.hasOwnProperty('operator')) {
                if ('operator' in s) {
                    var q1 = stack.pop()

                    if (q1.post_order == s.post_order - 2 && stack[stack.length - 1].post_order != s.post_order - 1) {
                        return recursive_build(stack) + ', ' + q1["search_pattern"];
                    } else {
                        return '{"' + s.operator.toLowerCase() + '":[' + recursive_build(stack) + ', ' + q1["search_pattern"] + "]}";
                    }
                }
            }
        }
    }

////////////////////////////////////////////////////////

	function recursive_build_mongo(stack, stack_auxiliary = []){
        console.log("recursive_build_mongo");
        //console.log(stack);
        //console.log("---------------", stack_auxiliary);

        var stack_length = stack.length;
        var stack_auxiliary_length = stack_auxiliary.length;

        if(stack_length <= 1 && stack_auxiliary_length == 0){
        
            var result = stack[0]['search_mongo']; 

            if(typeof result !== typeof ''){
                return JSON.stringify(result);  
            }else{
                return result;
            }
        }else{
            //if(stack[0].hasOwnProperty('operator')){
            if('operator' in stack[0]){
                var op2 = stack_auxiliary.pop();
                var op1 = stack_auxiliary.pop();

                console.log(op2, typeof op2);
                console.log(op1, typeof op1);

                

                //if(op2.hasOwnProperty('result')){
                if('result' in op2){
                    op2 = op2.search_mongo;
                }else{
                    op2 = JSON.stringify(op2.search_mongo);
                }

                //if(op1.hasOwnProperty('result')){
                if('result' in op1){
                    op1 = op1.search_mongo;
                }else{
                    op1 = JSON.stringify(op1.search_mongo);
                }

                var op = op1 + ',' + op2; 
                var s = '{"$'+ stack[0].operator.toLowerCase() + '":[' + op + ']}';
                var obj = {
                    result: true,
                    post_order:stack[0].post_order,
                    search_mongo: s
                };

                stack[0] = obj
                return recursive_build_mongo(stack, stack_auxiliary);
            }else{
                stack_auxiliary.push(stack[0]);
                stack.splice(0, 1);
                return recursive_build_mongo(stack, stack_auxiliary);
            }
        }
    }





/*
function recursive_build_mongo(stack, openOperator = 0, level = 0, previousOp = 0 ){
		console.log(recursive_build_mongo);
		console.log(stack);
		console.log(openOperator); 
		console.log(level); 
		console.log(previousOp); 
		var final_brac = "]}";

		if(stack.length == 1){
			if(stack[0].hasOwnProperty('operator')){
				console.log("IF1---", stack[0]["operator"]);
				return;
			}else{
				console.log("IF1---", stack[0]["search_mongo"], typeof stack[0]["search_mongo"]);
				if (openOperator == 1){
					if(previousOp == 0){
						return "," + JSON.stringify(stack[0]["search_mongo"]) + final_brac + final_brac;
					}else{
						return JSON.stringify(stack[0]["search_mongo"]) + final_brac + final_brac;
					}
				}else{
					if(previousOp == 0){
						if(level == 0){
							return JSON.stringify(stack[0]["search_mongo"]);
						}else{
							return "," + JSON.stringify(stack[0]["search_mongo"]) + final_brac;
						}
					}else{
						return JSON.stringify(stack[0]["search_mongo"]) + final_brac;
					}
				}
			}
		}else{
			if(stack.length == 2 && stack[0].hasOwnProperty('operator')){
				console.log("IF2---", stack[1]["search_mongo"])
				return JSON.stringify(stack[1]["search_mongo"]);
			}else{
				var s = stack[0];
				stack.shift();
				
				if(s.hasOwnProperty('operator')){
					console.log("IF3---", s);
					if(level == 0){
						level = level + 1;
						console.log('-------------------{"$'+ s.operator.toLowerCase() + '":[' );
						return '{"$'+ s.operator.toLowerCase() + '":[' +  recursive_build_mongo(stack, 0, level, 0);
					}else{
						console.log("ELSE1---", s);
						level = level + 1;
						if(openOperator == 1){
							console.log("$$$$$$$$$$$$$$$$$$$$" + final_brac + ',{"$'+ s.operator.toLowerCase() + '":[' )
							return final_brac + ',{"$'+ s.operator.toLowerCase() + '":[' +  recursive_build_mongo(stack, 1, level, 1);	
						}else{
							console.log('-----------------------{"$'+ s.operator.toLowerCase() + '":[' )
							return '{"$'+ s.operator.toLowerCase() + '":[' +  recursive_build_mongo(stack, 1, level, 1);
						}
					}
				}else{
					console.log("ELSE2---", s);
					level = level + 1;
					console.log(s["search_mongo"])
					if(previousOp == 0){
						return "," + JSON.stringify(s["search_mongo"]) + recursive_build_mongo(stack, openOperator == 1 ? 1 : 0, level, 0);	
					}else{
						return JSON.stringify(s["search_mongo"]) + recursive_build_mongo(stack, openOperator == 1 ? 1 : 0, level, 0);
					}
				}
			}
		}
	}
*/




/*
	function recursive_build_mongo(stack){
		console.log(recursive_build_mongo);
		console.log(stack);

		if(stack.length == 1){
			if(stack[0].hasOwnProperty('operator')){
				console.log("IF1---", stack[0]["operator"]);
				return;
			}else{
				console.log("IF1---", stack[0]["search_mongo"], typeof stack[0]["search_mongo"]);
				return JSON.stringify(stack[0]["search_mongo"]);
			}
		}else{
			//console.log(stack.length == 2);
			//console.log(stack[0].hasOwnProperty('operator'));
			if(stack.length == 2 && stack[0].hasOwnProperty('operator')){
				console.log("IF2---", stack[1]["search_mongo"])
				return JSON.stringify(stack[1]["search_mongo"]);
			}else{
				var s = stack[0];
				stack.shift();
				if(s.hasOwnProperty('operator')){
					if (stack[0].hasOwnProperty('operator')){
						var q1 = stack.pop()

						console.log("ZZZZZZZZZZEEEEEEEEEEE111111111111", JSON.stringify(q1["search_mongo"]));

						if (q1.post_order == s.post_order - 2 && stack[stack.length-1].post_order  != s.post_order - 1){
							console.log("IF3---")
							return recursive_build_mongo(stack) + ', ' + JSON.stringify(q1["search_mongo"]);	
						}else{
							//if (s.operator.toLowerCase() == 'or'){
								console.log("IF4---");
								console.log('{"$'+ s.operator.toLowerCase() + '":[' +  recursive_build_mongo(stack) + ', ' + JSON.stringify(q1["search_mongo"]) + "]}");
								return '{"$'+ s.operator.toLowerCase() + '":[' +  recursive_build_mongo(stack) + ', ' + JSON.stringify(q1["search_mongo"]) + "]}";
							//}else{
							//	console.log("IF5---", q1["search_mongo"], typeof q1["search_mongo"])
							//	return '{' + recursive_build_mongo(stack) + ', ' + JSON.stringify(q1["search_mongo"]) + '}';
							//}
						}
					}else{
						var q1 = stack[0];
						stack.shift();

						console.log("ZZZZZZZZZZEEEEEEEEEEE2222222222222", JSON.stringify(q1["search_mongo"]));
						return '{"$'+ s.operator.toLowerCase() + '":[' +  recursive_build_mongo(stack) + ', ' + JSON.stringify(q1["search_mongo"]) + "]}";
					}
				}
			}
		}
	}
*/

////////////////////////////////////////////////////////
	function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }

	function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


	var query_equal = function (attribute_name_id, value, attribute_values,post_order, operator, cb){
		console.log(query_equal);
		//console.log(operator);
		
		//if (Number.isInteger(value)){
		//	console.log("111111111111111111111111")
		//	var val = parseInt(value);
		//}else{
		//	console.log('222222222222222222222222')
		//	var val = '"' + value + '"';
		//}
		//var val = value;

		//console.log("VVVVVVVVVVVVVVv", value, typeof value);
		//console.log("VVVVVVVVVVVVVVv", val, typeof val);


		if(typeof value === 'string'){
			//console.log("111111111111111111111111");
			//console.log(isNumeric(value));
			if(isNumeric(value)){
				//console.log("11111333333333333333");
				var val = parseInt(value);
			}else{
				//console.log("11111114444444444444")
				var val = '"' + value + '"';
			}
		}else{
			//console.log('222222222222222222222222')
			var val = value;
		}

		//console.log("VVVVVVVVVVVVVVv", val, typeof val);

		if(operator == "="){
			var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":' + val + '}}';
			//console.log(query, typeof query);
			var obj_query = JSON.parse(query);
			var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":' + val + '}';
			var validations = [];

			//console.log("%%%%%%%%%%%%%%%%%%%%%1", search_mongo["values.4.value"], typeof search_mongo)
			//console.log("%%%%%%%%%%%%%%%%%%%%%1", obj_query, typeof obj_query)
			var obj_resp = {
							post_order: post_order,
							query: query,
							search_pattern: search_pattern,
							search_mongo: JSON.parse(search_pattern)
							}

			validations.push(obj_resp);
			cb(validations);
		}else{
			if(operator == ">"){
				var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":{"gt":' + parseInt(val) + '}}}';
				//console.log(query, typeof query);
				var obj_query = JSON.parse(query);
				var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":{"gt":' + parseInt(val) + '}}';
				//var search_mongo = JSON.parse('{"values.' + attribute_name_id.toString() + '.value":' + JSON.stringify({$gt: val})+'}');

				var search_mongo = JSON.parse('{"values.' + attribute_name_id.toString() + '.value":{"$gt":' + parseInt(val)+'}}');

				var validations = []

				//console.log("%%%%%%%%%%%%%%%2", search_mongo["values.4.value"], typeof search_mongo)
				//console.log("%%%%%%%%%%%%%%%%%%%%%1", obj_query, typeof obj_query)
				var obj_resp = {
								post_order: post_order,
								query: query,
								search_pattern: search_pattern,
								search_mongo: search_mongo
								}

				validations.push(obj_resp);
				cb(validations);
			}else{
				if(operator == "<"){
					var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":{"lt":' + parseInt(val) + '}}}';
					//console.log(query, typeof query);
					var obj_query = JSON.parse(query);
					var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":{"lt":' + parseInt(val) + '}}';
					//var search_mongo = JSON.parse('{"values.' + attribute_name_id.toString() + '.value":' + JSON.stringify({$lt: val})+'}');


					var search_mongo = JSON.parse('{"values.' + attribute_name_id.toString() + '.value":{"$lt":' + parseInt(val)+'}}');	
					var validations = []

					//console.log("%%%%%%%%%%%%%%%%3", search_mongo["values.4.value"], typeof search_mongo)
					//console.log("%%%%%%%%%%%%%%%%%%%%%1", obj_query, typeof obj_query)
					var obj_resp = {
									post_order: post_order,
									query: query,
									search_pattern: search_pattern,
									search_mongo: search_mongo
									}

					validations.push(obj_resp);
					cb(validations);
				}else{
					if(operator == "<>"){

						if (typeof(val) == "number") {

							val = '"' + val + '"';

						}

						var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":{"neq":' + val + '}}}';
						//console.log(query, typeof query);
						var obj_query = JSON.parse(query);
						var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":{"neq":' + val + '}}';
						//var search_mongo = JSON.parse('{"values.' + attribute_name_id.toString() + '.value":' + JSON.stringify({$ne: val})+'}');

						var search_mongo = JSON.parse('{"values.' + attribute_name_id.toString() + '.value":{"$ne":' + val+'}}');

						//console.log("%%%%%%%%%%%%%%4", search_mongo["values.4.value"], typeof search_mongo)
						//console.log("%%%%%%%%%%%%%%%%%%%%%1", obj_query, typeof obj_query)			
						var validations = []
						var obj_resp = {
										post_order: post_order,
										query: query,
										search_pattern: search_pattern,
										search_mongo: search_mongo
										}

						validations.push(obj_resp);
						cb(validations);
					}
				}
			}
		}		
	}



	function get_logical(request_rule_id, conditional_logical, operator_logical, cb){
		console.log(get_logical);
		var validations = [];

		conditional_logical.find({where: {rule_id: request_rule_id}} , function(err, results){
			if (err) {
				throw err;
			}

			var count = results.length - 1;

			if(results.length > 0){

				results.forEach(function(result) {
					operator_logical.find({where: {external_id: result.operator_logical_id}} , function(err, r){
						if (err) {
							throw err;
						}

						r.forEach(function(op) {
							if(r){
								var operator = op.operator;
								var post_order = result.post_order;

								var obj_resp = {
									operator: operator,
									post_order: post_order
								}

								validations.push(obj_resp);
								if(count == 0){
									cb(validations);

								}
								count--;
							}else{
								count--;
							}
						});
					});
				});
					
			}else{
				cb(validations);
			}
		});
	}

/*
	function get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, cb){
		console.log(get_relational);

		conditions_relational.find({where: {rule_id: request_rule_id}} , function(err, results){
			if (err) {
				throw err;
			}

			var validations = [];

			var count = results.length - 1;
			//console.log(results);

			if(results.length > 0){
				results.forEach(function(result) {

					operator_relational.findOne({where: {external_id: result.operator_relational_id}} , function(err, r){
						if (err) {
							throw err;
						}

						if(r){
							var operator = r.operator;
							var attribute_name_id = result.attribute_name_id;
							var value = result.value;
							var post_order = result.post_order;

							//console.log('OOOOOOOOOOOPPPPPPPPPPPPPPPPP', operator);

							//if(operator == "="){
							query_equal(attribute_name_id, value, attribute_values, post_order, operator,  function(vals){
								//console.log("vals", vals);
								//console.log("validations", validations);
								/*if(operator == ">"){
									//console.log('IIIIIIIIIIIIIFFFFFFFFF111111111111');
									//console.log(validations);
									//console.log(validations.length);

									var changed = false;
									for(var i = 0; i < validations.length; i++){
										//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$", validations[i]["search_pattern"]);
										if(validations[i]["search_pattern"].includes('{"values.' + attribute_name_id.toString() + '.value":{"lt":')){
											//console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT1111111111111111111111");

											var inicio = validations[i]["search_pattern"].slice(0, validations[i]["search_pattern"].indexOf('"lt"'));
											var fim = validations[i]["search_pattern"].slice(validations[i]["search_pattern"].indexOf('"lt"'), -2);

											//console.log(inicio);
											//console.log(fim);
											var query = '{"where":' + inicio + '"gt":"' + parseInt(value) +'", '+ fim + '}}}';

											//console.log("--------", query);

											var obj_query = JSON.parse(query);
											var search_pattern = inicio + '"gt":"' + parseInt(value) +'", '+ fim + '}}';


											//console.log("@@@@@@@@@@@@@@@@", validations[i]["search_mongo"], typeof validations[i]["search_mongo"])


											var obj_mongo_join = Object.assign(validations[i]["search_mongo"]["values." + attribute_name_id.toString() + ".value"],{$gt: parseInt(value)});



											validations[i]["search_pattern"] = search_pattern;
											validations[i]["query"] = query;
											validations[i]["search_mongo"]["values." + attribute_name_id.toString() + ".value"] = obj_mongo_join;

											changed = true;

											//console.log(validations[i]["search_mongo"]);

											//cb(validations);						
										}
									}

									if(changed == false){
										Array.prototype.push.apply(validations,vals);
									}

								}else{
									if(operator == "<"){
										//console.log('IIIIIIIIIIIIIFFFFFFFFF2222222222');
										var changed = false;
										for(var i = 0; i < validations.length; i++){
											//console.log("####################################", validations[i]["search_pattern"]);
											if(validations[i]["search_pattern"].includes('{"values.' + attribute_name_id.toString() + '.value":{"gt":')){
												//console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT1111111111111111111111222222222222222222222222");
												var query = '{"where":' + validations[i]["search_pattern"].slice(0, -2) + ', "lt":"' + parseInt(value) + '"}}}';
												//console.log(query);
												var obj_query = JSON.parse(query);
												var search_pattern = validations[i]["search_pattern"].slice(0, -2) + ', "lt":"' + parseInt(value) + '"}}';



												//console.log("@@@@@@@@@@@@@@@@", validations[i]["search_mongo"], typeof validations[i]["search_mongo"])
												var obj_mongo_join = Object.assign(validations[i]["search_mongo"]["values." + attribute_name_id.toString() + ".value"],{$lt:parseInt(value)});


												validations[i]["search_pattern"] = search_pattern;
												validations[i]["query"] = query
												validations[i]["search_mongo"]["values." + attribute_name_id.toString() + ".value"] = obj_mongo_join;

												//console.log(validations[i]["search_mongo"]);

												changed = true;

												//cb(validations);						
											}
										}

										if(changed == false){
											Array.prototype.push.apply(validations,vals);
										}
									}
									else{
										//									console.log('EEEEEEEEEEEEEEEEEEEEEEEEe');
										Array.prototype.push.apply(validations,vals);
									}

								}
								Array.prototype.push.apply(validations,vals);
								console.log("count--", count);
								if(count == 0){
									//console.log("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", validations);
									cb(validations);
								} 
								count--;
							});
						}
					});
				});
			}else{
				cb(validations);
			}
		});
		
	}
*/

	function get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, cb) {
        console.log(get_relational);
        console.log(request_rule_id);

        conditions_relational.find({
            where: {
                rule_id: request_rule_id
            }
        }, function(err, results) {
            if (err) {
                throw err;
            }

            var validations = [];

            var count = results.length - 1;
            //console.log(results);

            if (results.length > 0) {
                results.forEach(function(result) {

                    operator_relational.findOne({
                        where: {
                            external_id: result.operator_relational_id
                        }
                    }, function(err, r) {
                        if (err) {
                            throw err;
                        }

                        if (r) {
                            //if (typeof r !== 'undefined' && r !== null) {
                            var operator = r.operator;
                            var attribute_name_id = result.attribute_name_id;
                            var value = result.value;
                            var post_order = result.post_order;

                            //console.log('OOOOOOOOOOOPPPPPPPPPPPPPPPPP', operator);

                            //if(operator == "="){
                            query_equal(attribute_name_id, value, attribute_values, post_order, operator, function(vals) {
                                Array.prototype.push.apply(validations, vals);
                                console.log("count--", count);
                                if (count == 0) {
                                    //console.log("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", validations);
                                    cb(validations);
                                }
                                count--;
                            });
                        } else {
                            var attribute_name_id = result.attribute_name_id;
                            var value = result.value;
                            var post_order = result.post_order;

                            if (typeof value === 'string') {
                                //console.log("111111111111111111111111");
                                //console.log(isNumeric(value));
                                if (isNumeric(value)) {
                                    //console.log("11111333333333333333");
                                    var val = parseInt(value);
                                } else {
                                    //console.log("11111114444444444444")
                                    var val = '"' + value + '"';
                                }
                            } else {
                                //console.log('222222222222222222222222')
                                var val = value;
                            }

                            var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":' + val + '}}';
                            //console.log(query, typeof query);
                            var obj_query = JSON.parse(query);
                            var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":' + val + '}';

                            var obj_resp = {
                                post_order: post_order,
                                query: query,
                                search_pattern: search_pattern,
                                search_mongo: JSON.parse(search_pattern)
                            }

                            validations.push(obj_resp);

                            cb(validations);
                        }
                    });
                });
            } else {
            	/*
                //cb(validations);
                var attribute_name_id = result.attribute_name_id;
                var value = result.value;
                var post_order = result.post_order;

                if (typeof value === 'string') {
                    //console.log("111111111111111111111111");
                    //console.log(isNumeric(value));
                    if (isNumeric(value)) {
                        //console.log("11111333333333333333");
                        var val = parseInt(value);
                    } else {
                        //console.log("11111114444444444444")
                        var val = '"' + value + '"';
                    }
                } else {
                    //console.log('222222222222222222222222')
                    var val = value;
                }

                var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":' + val + '}}';
                //console.log(query, typeof query);
                var obj_query = JSON.parse(query);
                var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":' + val + '}';

                var obj_resp = {
                    post_order: post_order,
                    query: query,
                    search_pattern: search_pattern,
                    search_mongo: JSON.parse(search_pattern)
                }

                validations.push(obj_resp);
                */
                cb(validations);
            }
        });

    }

	function get_rule_info(request_rule_id, rules, cb){
		console.log(get_rule_info);

		rules.find({where: {external_id: request_rule_id}} , function(err, results){
			if (err) {
				throw err;
			}

			console.log("results", results);

			var validations = [];

			if(results.length > 0){
				validations.push(results.att_g_val_identification_validation);
				//console.log("results.att_g_val_identification_validation", results.att_g_val_identification_validation)
				cb(validations);
			}else{
				cb(validations);
			}
		});
	}


	/////////////////////////////////////////////////////////////////////////

	Ektixvalidationrequest.distinctRule_validation = function(request_array_rules_id, identification, distinct_tag, cb) {
		var start = now()

		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;


		var array_validations = []

		console.log("request_array_rules_id", request_array_rules_id);

		var request_size = request_array_rules_id.length;
		var count = 1;
		var querys = [];

		//console.log("request_size", request_size);

		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";
		var obj_request = {
			"request_rule_id": request_array_rules_id,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		request_id = inserted.id;
      		console.log("request_id: ", request_id);

      		if(request_size == 1){ // Se for apenas uma regra
				//console.log("IIIIIIIIIIIIFFFFFFFFFFFFFFFFFFFFFFFFFFf");
				var request_rule_id = request_array_rules_id[0];

				/////////////////////////
				get_rule_info(request_rule_id, rules, function(rule_info){
					//console.log("rule_info", rule_info);
	      			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
	      				//console.log("validations_relational", validations_relational);
						if(validations_relational.length > 0){ 
							get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
								var begin = 0;

								calculate_expression_distinct(validations_relational, validations_logical, attribute_values, identification, distinct_tag, function(final_result){
									date = Moment().utc("Europe/Lisbon").format();

									var obj_response = {
										"request_id": request_id,
									    "response": final_result.length,
									    "created_date": date,
									    "modified_date": date
									}

									validation_response.create(obj_response, function(err, inserted) {
							      		if (err) throw err;

							      		var response_id = inserted.id;
		      							console.log("response_id: ", response_id);

		      							var end = now();
										console.log("Tempo processamento: ", (end-start).toFixed(3));
		      							
		      							console.log("!########################!");
									});	
									return cb(null, final_result);					
								});
							});
						}else{
							return cb(null, []);
						}
					});
      			});
      		}else{ // se forem várias regras
      			//cb(null, []);
      			console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEELLLLLLLLLLLLLL");
				request_array_rules_id.forEach(function(request_rule_id) {
					//console.log("rules_id", request_rule_id);

					get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
						get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
							build_querys(validations_relational, validations_logical, function(querys_result){

								//console.log(count);
								//console.log(querys_result);

								if(querys_result.length > 0){
									//console.log("TTTTTTTTTTTTTTT");
									querys.push(querys_result);
								}

								if(count == request_size){
									//console.log("querys", querys);
									if(querys.length > 0){
										execute_querys_distinct(querys, attribute_values, identification, distinct_tag, function(final_result){

											date = Moment().utc("Europe/Lisbon").format();
											var obj_response = {
												"request_id": request_id,
											    "response": final_result.length,
											    "created_date": date,
											    "modified_date": date
											}

											validation_response.create(obj_response, function(err, inserted) {
									      		if (err) throw err;

									      		var response_id = inserted.id;
				      							console.log("response_id: ", response_id);

				      							var end = now();
												console.log("Tempo processamento: ", (end-start).toFixed(3));
		      							
		      									console.log("!########################!");

											});	
											return cb(null, final_result);
										});
									} else{
										return cb(null, querys);
									}
								}
								count++;
							});
						});
					});
				});
			}
		});
	}


	function calculate_expression_distinct(validations_relational, validations_logical, attribute_values, identification, distinct_tag, cb){
		console.log(calculate_expression_distinct);

		var stack = [];

		Array.prototype.push.apply(stack,validations_relational);
		Array.prototype.push.apply(stack,validations_logical);

		stack.sort(function(a, b) {
    		//return b.post_order - a.post_order;
    		return a.post_order - b.post_order;
		});

		var query = "";
		var close = "";
		var operator_before = 1;

		var query_mongo = recursive_build_mongo(stack);
		//console.log("---------------------------------", query_mongo, typeof query_mongo);

		var identification_string = "";

		var identification_mongo = "";

		if(typeof identification  !== 'undefined'){
			Object.keys(identification).forEach(function (k) {
				//console.log("KKKKKKKKKKKKKK", k);
				identification_string +=  '{"identification.'+ k +'":"'+ identification[k] +'"},';
				identification_mongo +=  '{"identification.'+ k +'":"'+ identification[k] +'"},';
			});

			query = '{"and":['+ identification_string  + query + ']}';
			query_mongo = '{"$and":['+ identification_mongo  + query_mongo + ']}';
		}

		//console.log("---------------------------------V_distinct", JSON.parse(query_mongo), typeof query_mongo);

		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;

		  db.collection("ektix_attribute_values").distinct(distinct_tag, JSON.parse(query_mongo),function(err, result) {
		    if (err) throw err;
		    
		    console.log(distinct_tag);
		    console.log(JSON.parse(query_mongo));
		    console.log("############", typeof result);
		    //console.log(Array.isArray(result));
		    //console.log("############");

		    var xp = result.map(function(num){
		    	return parseInt(num);
		    });

		    console.log(xp.sort(function(a, b){return a-b}));

		    db.close();
		    cb(xp);
		  });
		});
	}



	function execute_querys_distinct(querys, attribute_values, identification, distinct_tag, cb){
		console.log(execute_querys_distinct);
		//console.log(querys);

		var count = 1;
		var querys_size =  querys.length;

		var result = "";
		var close = "]}";
		var and_open = 0;

		if(querys.length == 1){
			result = querys[0];
			//console.log("11111111111111111111111111111111111", result);
		}else{
			querys.forEach(function(q) {
				//console.log(q);
				//console.log(result); 

				if(and_open == 0){
					//console.log("IIIIIIIIIIIIIIIIIIIIIIIIIII");

					if(result){
						//result = '{"or":[' + q + ", " + result + close;
						result = '{"$or":[' + q + ", " + result + close;
					}else{
						//result = '{"or":[' + q + ", ";
						result = '{"$or":[' + q + ", ";
						and_open = 1;	
					}
				}else{
					//console.log("EEEEEEEEEEEEEEEEEEEEEEEEEE");
					if(and_open == 1){
						//console.log("IIIIII2222222222222222");
						result = result + q + close;
						and_open = 0;
					}
				}

				//console.log("------------------", result);
				count++;
			});
		}
		
		//var limit_string = "";
		//var skip_string = "";
		//var identification_string = "";


		//var limit_mongo = 0;
		//var skip_mongo = 0;
		var identification_mongo = "";

		/*if(typeof limit  !== 'undefined'){
			limit_string = '"limit":"' + String(limit) + '",';
			//console.log(limit_string);
			limit_mongo = parseInt(limit);
		}

		if(typeof skip  !== 'undefined'){
			skip_string = '"skip":"' + String(skip) + '",';
			//console.log(skip_string);
			skip_mongo = parseInt(skip);
		}*/

		//console.log("identification", identification);


		if(typeof identification  !== 'undefined'){
			Object.keys(identification).forEach(function (k) {
				//identification_string += '{"identification.'+ k +'":"'+ identification[k] +'"},';
				identification_mongo +=  '{"identification.'+ k +'":"'+ identification[k] +'"},';
			});

			//result = '{"and":['+ identification_string  + result + ']}';
			result = '{"$and":['+ identification_mongo  + result + ']}';
		}

		//console.log('{"where":'+ result +','+' "order":"attribute_group_value_id ASC"}');		

		//var final_query = JSON.parse('{"where":'+ result +','+' "order":"attribute_group_value_id ASC"}');

		//attribute_values.find( final_query , function(err, results_query){
		//	if(err) throw err;

		//	cb(results_query);

		//});

		//console.log("---------------------------------V2", result, typeof result);
		//console.log("---------------------------------V2", distinct_tag, typeof distinct_tag);

		/*MongoClient.connect(url, function(err, db) {
		  if (err) throw err;

		  //var query_mongo = { address: /^S/ };

		  db.collection("ektix_attribute_values").find(JSON.parse(result)).sort({attribute_group_value_id: 1}).limit(limit_mongo).skip(skip_mongo).toArray(function(err, result) {
		    if (err) throw err;
		    //console.log(result);
		    db.close();
		    cb(result);
		  });
		});*/

		MongoClient.connect(url, function(err, db) {
		  if (err) throw err;

		  db.collection("ektix_attribute_values").distinct(distinct_tag, JSON.parse(result), {"sort": ['values.4.value', 'asc']},function(err, result_dist) {
		    if (err) throw err;
		    
		    //console.log("############", typeof result_dist);
		    //console.log(Array.isArray(result));
		    //console.log("############");

		    var xp = result_dist.map(function(num){
		    	return parseInt(num);
		    });

		    //console.log(xp.sort(function(a, b){return a-b}));

		    db.close();
		    cb(xp);
		  });
		});
	}

	/////////////////////////////////////////////////////////////////////////

	Ektixvalidationrequest.multiRule_bool = function(request_array_rules_id, limit, skip, identification, cb) {
		console.log("multiRule_bool");
		var start = now()

		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;
		var cache = app.models.EktixSearchCache;


		var array_validations = [];
		var limit = 1;

		console.log("request_array_rules_id", request_array_rules_id);

		var request_size = request_array_rules_id.length;
		var count = 1;
		var querys = [];

		//console.log("request_size", request_size);

		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";
		var obj_request = {
			"request_rule_id": request_array_rules_id,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "identification": identification,
		    "limit": limit,
		    "skip": skip,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		request_id = inserted.id;
      		console.log("request_id: ", request_id);

      		if(request_size < 1){ // Se for apenas uma regra
				//console.log("IIIIIIIIIIIIFFFFFFFFFFFFFFFFFFFFFFFFFFf");
				var request_rule_id = request_array_rules_id[0];

				/////////////////////////
				get_rule_info(request_rule_id, rules, function(rule_info){
					//console.log("rule_info", rule_info);
	      			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
	      				//console.log("validations_relational", validations_relational);
						if(validations_relational.length > 0){ 
							get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
								var begin = 0;

								calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, identification, function(final_result){
									date = Moment().utc("Europe/Lisbon").format();
									var request_bool = 0;
									var result_send = {};

									if(final_result.length > 0){
										request_bool = 1;
										result_send[request_rule_id] =  request_bool;

									}else{
										result_send[request_rule_id] =  request_bool;
									}

									var obj_response = {
										"request": "multiRule_bool",
										"request_rule_id": request_array_rules_id,
										"response_bool": result_send,	
										"request_id": request_id,
									    "response": "0",
									    "identification": identification,
		    							"limit": limit,
		    							"skip": skip,
									    "created_date": date,
									    "modified_date": date
									}

									var obj_cache = {
										"search_pattern": identification,
										"search_result": result_send
									};

									validation_response.create(obj_response, function(err, inserted) {
							      		if (err) throw err;

							      		var response_id = inserted.id;
		      							console.log("response_id: ", response_id);

		      							var end = now();
										console.log("Tempo processamento: ", (end-start).toFixed(3));

		      							console.log("!########################!");
									});	

									cache.create(obj_cache, function(err, inserted) {
							      		if (err) throw err;
							      	});

									return cb(null, result_send);					
								});
							});
						}else{
							return cb(null, result_send);
						}
					});
      			});
				//////////////////
      		}else{ // se forem várias regras
      			console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEELLLLLLLLLLLLLL");
      			
				var result_send = {};

				request_array_rules_id.forEach(function(request_rule_id) {
					//console.log("rules_id", request_rule_id);

					var request_bool = 0;


					MongoClient.connect(url, function(err, db) {
					  if (err) throw err;

					  var distinct_tag = "search_result." + request_rule_id;
					  var req_rule = "search_result." + String(request_rule_id);
					  var query = '{"$and":[{"'+ req_rule +'" :{"$exists" : true}}, {"search_pattern" :'+ JSON.stringify(identification) + '}]}'

					  console.log("query", query);
					  console.log("distinct_tag", distinct_tag);

					  db.collection("ektix_search_cache").distinct(distinct_tag, JSON.parse(query),function(err, result_dist) {
					    if (err) throw err;
					    
					    console.log(result_dist);
					    console.log("############", typeof result_dist);
					    //console.log(Array.isArray(result));
					    //console.log("############");

					    //var xp = result_dist.map(function(num){
					    //	return parseInt(num);
					    //});

					    //console.log(xp.sort(function(a, b){return a-b}));

					    db.close();
					    //cb(xp);

					    if(result_dist.length > 0){
					    	//if(result_dist[0] == 1){
					    		result_send[request_rule_id] = result_dist[0];
					    	//}

					    	console.log("--------------", result_send);
					    	if(count == request_size){

					    		var end = now();
								console.log("Tempo processamento: ", (end-start).toFixed(3));
      							console.log("!########################!");

					    		return cb(null, result_send);
					    	}
					    	count++;
					    }else{
							get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
								get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
									//build_querys(validations_relational, validations_logical, function(querys_result){

										//console.log(count);
										//console.log(querys_result);

									calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, identification, function(final_result){
										date = Moment().utc("Europe/Lisbon").format();
										var request_bool = 0;
										//var result_send = {};

										if(final_result.length > 0){
											request_bool = 1;
											result_send[request_rule_id] =  request_bool;

										}else{
											result_send[request_rule_id] =  request_bool;
										}

										if(count == request_size){
											var obj_response = {
												"request": "multiRule_bool",
												"request_rule_id": request_array_rules_id,
												"response_bool": result_send,	
												"request_id": request_id,
											    "response": "0",
											    "identification": identification,
				    							"limit": limit,
				    							"skip": skip,
											    "created_date": date,
											    "modified_date": date
											}

											var obj_cache = {
												"search_pattern": identification,
												"search_result": result_send
											};

											validation_response.create(obj_response, function(err, inserted) {
									      		if (err) throw err;

									      		var response_id = inserted.id;
				      							console.log("response_id: ", response_id);

				      							var end = now();
												console.log("Tempo processamento: ", (end-start).toFixed(3));

				      							console.log("!########################!");
											});

											cache.create(obj_cache, function(err, inserted) {
									      		if (err) throw err;
									      	});

											return cb(null, result_send);
										}
										count++;					
									});
									//if(count == request_size){
									//	return cb(null, result_send);
									//}
									//count++;
									//});
								});
							});
						}
					  });
					});
				});
			}
		});
		//console.log(querys);
	}





	////////////////////////////////////////////////////////////////////////


	Ektixvalidationrequest.remoteMethod('rule_validation', {
		description:"Método para validar regras",
		http:{
			path:'/rule_validation',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_rule_id', type : 'number', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

	Ektixvalidationrequest.remoteMethod('multiRule_validation', {
		description:"Método para validar várias regras em simultâneo",
		http:{
			path:'/multiRule_validation',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_array_rules_id', type : 'array', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false},
			{ arg : 'identification', type : 'object', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

	Ektixvalidationrequest.remoteMethod('distinctRule_validation', {
		description:"",
		http:{
			path:'/distinctRule_validation',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_array_rules_id', type : 'array', required: true},
			{ arg : 'identification', type : 'object', required: false},
			{ arg : 'distinct_tag', type : 'string', required: true}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixvalidationrequest.remoteMethod('multiRule_bool', {
		description:"Método para validar os resultados de várias regras",
		http:{
			path:'/multiRule_bool',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_array_rules_id', type : 'array', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false},
			{ arg : 'identification', type : 'object', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});
};
