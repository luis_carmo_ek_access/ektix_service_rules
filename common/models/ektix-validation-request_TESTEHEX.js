'use strict';

module.exports = function(Ektixvalidationrequest) {

	var async = require('async');
	var _ = require('underscore');
	var Moment = require('moment-timezone');

	Ektixvalidationrequest.rule_validation = function(request_rule_id, limit, skip, cb) {
		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;

		var validations = [];
		
		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";

  		var array_insert = [];
		array_insert.push(request_rule_id);

		var obj_request = {
			"request_rule_id": array_insert,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		console.log("Request: ", request_rule_id);
      		request_id = inserted.id;
      		console.log("request_id: ", request_id);
      	
      		///////////////////////////////////////////
			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
				//validations = validations_relational;

				console.log("validations_relational", validations_relational);

				if(validations_relational.length > 0){ 
					get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
						var begin = 0;

						calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, function(final_result){

							date = Moment().utc("Europe/Lisbon").format();
							

							var obj_response = {
								"request_id": request_id,
							    "response": final_result.length,
							    "created_date": date,
							    "modified_date": date
							}

							validation_response.create(obj_response, function(err, inserted) {
					      		if (err) throw err;

					      		var response_id = inserted.id;
      							console.log("response_id: ", response_id);

							});	

							return cb(null, final_result);					
						});
					});
				}else{
					return cb(null, []);
				}
			});
			//////////////////////////////////////////////
		});
	}

/*
	Ektixvalidationrequest.multiRule_validation = function(request_array_rules_id, limit, skip, cb) {
		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;


		var array_validations = []

		console.log("request_array_rules_id", request_array_rules_id);

		var request_size = request_array_rules_id.length;
		var count = 1;
		var querys = [];

		//console.log("request_size", request_size);

		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";
		var obj_request = {
			"request_rule_id": request_array_rules_id,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		request_id = inserted.id;
      		console.log("request_id: ", request_id);

      		if(request_size == 1){ // Se for apenas uma regra
				//console.log("IIIIIIIIIIIIFFFFFFFFFFFFFFFFFFFFFFFFFFf");
				var request_rule_id = request_array_rules_id[0];

				/////////////////////////
				get_rule_info(request_rule_id, rules, function(rule_info){
					console.log("rule_info", rule_info);
	      			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
	      				//console.log("validations_relational", validations_relational);
						if(validations_relational.length > 0){ 
							get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
								var begin = 0;

								calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, function(final_result){
									date = Moment().utc("Europe/Lisbon").format();

									var obj_response = {
										"request_id": request_id,
									    "response": final_result.length,
									    "created_date": date,
									    "modified_date": date
									}

									validation_response.create(obj_response, function(err, inserted) {
							      		if (err) throw err;

							      		var response_id = inserted.id;
		      							console.log("response_id: ", response_id);
									});	
									return cb(null, final_result);					
								});
							});
						}else{
							return cb(null, []);
						}
					});
      			});
				//////////////////
      		}else{ // se forem várias regras
      			console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEELLLLLLLLLLLLLL");
				request_array_rules_id.forEach(function(request_rule_id) {
					console.log("rules_id", request_rule_id);

					get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
						get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
							build_querys(validations_relational, validations_logical, function(querys_result){

								//console.log(count);
								//console.log(querys_result);

								if(querys_result.length > 0){
									//console.log("TTTTTTTTTTTTTTT");
									querys.push(querys_result);
								}

								if(count == request_size){
									//console.log("querys", querys);
									if(querys.length > 0){
										execute_querys(querys, limit, skip, attribute_values, function(final_result){

											date = Moment().utc("Europe/Lisbon").format();
											var obj_response = {
												"request_id": request_id,
											    "response": final_result.length,
											    "created_date": date,
											    "modified_date": date
											}

											validation_response.create(obj_response, function(err, inserted) {
									      		if (err) throw err;

									      		var response_id = inserted.id;
				      							console.log("response_id: ", response_id);

											});	
											return cb(null, final_result);
										});
									} else{
										return cb(null, querys);
									}
								}
								count++;
							});
						});
					});
				});
			}
		});
		//console.log(querys);
	}
*/

	Ektixvalidationrequest.multiRule_validation = function(request_array_rules_id, limit, skip, identification, cb) {
		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;


		var array_validations = []

		console.log("request_array_rules_id", request_array_rules_id);

		var request_size = request_array_rules_id.length;
		var count = 1;
		var querys = [];

		//console.log("request_size", request_size);

		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";
		var obj_request = {
			"request_rule_id": request_array_rules_id,
		    "request_template_id": 0,
		    "request_product_id": 0,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		request_id = inserted.id;
      		console.log("request_id: ", request_id);

      		if(request_size == 1){ // Se for apenas uma regra
				//console.log("IIIIIIIIIIIIFFFFFFFFFFFFFFFFFFFFFFFFFFf");
				var request_rule_id = request_array_rules_id[0];

				/////////////////////////
				get_rule_info(request_rule_id, rules, function(rule_info){
					//console.log("rule_info", rule_info);
	      			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
	      				//console.log("validations_relational", validations_relational);
						if(validations_relational.length > 0){ 
							get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
								var begin = 0;

								calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, identification, function(final_result){
									date = Moment().utc("Europe/Lisbon").format();

									var obj_response = {
										"request_id": request_id,
									    "response": final_result.length,
									    "created_date": date,
									    "modified_date": date
									}

									validation_response.create(obj_response, function(err, inserted) {
							      		if (err) throw err;

							      		var response_id = inserted.id;
		      							console.log("response_id: ", response_id);
									});	
									return cb(null, final_result);					
								});
							});
						}else{
							return cb(null, []);
						}
					});
      			});
				//////////////////
      		}else{ // se forem várias regras
      			console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEELLLLLLLLLLLLLL");
				request_array_rules_id.forEach(function(request_rule_id) {
					//console.log("rules_id", request_rule_id);

					get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
						get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
							build_querys(validations_relational, validations_logical, function(querys_result){

								//console.log(count);
								//console.log(querys_result);

								if(querys_result.length > 0){
									//console.log("TTTTTTTTTTTTTTT");
									querys.push(querys_result);
								}

								if(count == request_size){
									//console.log("querys", querys);
									if(querys.length > 0){
										execute_querys(querys, limit, skip, attribute_values, identification, function(final_result){

											date = Moment().utc("Europe/Lisbon").format();
											var obj_response = {
												"request_id": request_id,
											    "response": final_result.length,
											    "created_date": date,
											    "modified_date": date
											}

											validation_response.create(obj_response, function(err, inserted) {
									      		if (err) throw err;

									      		var response_id = inserted.id;
				      							console.log("response_id: ", response_id);

											});	
											return cb(null, final_result);
										});
									} else{
										return cb(null, querys);
									}
								}
								count++;
							});
						});
					});
				});
			}
		});
		//console.log(querys);
	}


/*
	Ektixvalidationrequest.get_zones = function(product_id, limit, skip, cb) {
		var app = Ektixvalidationrequest.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var attribute_values = app.models.EktixAttributeValues;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;

		var array_validations = []

		console.log("product_id", product_id);

		var querys = [];

		var date = Moment().utc("Europe/Lisbon").format();
  		var request_id = "";
		var obj_request = {
			"request_rule_id": 0,
		    "request_template_id": 0,
		    "request_product_id": product_id,
		    "created_date": date,
		    "modified_date": date
		}

		Ektixvalidationrequest.create(obj_request, function(err, inserted) {
      		if (err) throw err;

      		request_id = inserted.id;
      		console.log("request_id: ", request_id);


      		rules.find({"where": {"att_g_val_identification_validation.product_id": "325"}, "fields":{"external_id": true}}, function(err, results) {
      			if (err) throw err;

      			var rules_ids = results.map(function(a) {return a.external_id;});

      			console.log(rules_ids);

      			var request_size = rules_ids.length;
				var count = 1;
				var querys = [];

				rules_ids.forEach(function(request_rule_id) {
				console.log("rules_id", request_rule_id);


	      			get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, function(validations_relational){
						if(validations_relational.length > 0){
							get_logical(request_rule_id, conditional_logical, operator_logical, function(validations_logical){
								build_querys(validations_relational, validations_logical, function(querys_result){ 
									querys.push(querys_result);

									if(count == request_size){
										execute_querys(querys, limit, skip, attribute_values, function(final_result){

											get_zones_result(final_result, function(zones){
												//if (err) throw err;

												//console.log("zones------------", zones)

												date = Moment().utc("Europe/Lisbon").format();
												var obj_response = {
													"request_id": request_id,
												    "response": final_result.length,
												    "created_date": date,
												    "modified_date": date
												}

												validation_response.create(obj_response, function(err, inserted) {
										      		if (err) throw err;

										      		var response_id = inserted.id;
					      							console.log("response_id: ", response_id);
												});	

												return cb(null, zones);

											});
										});
									}
									count++;
								});
							});
						}	
					});
				});
      		});
      	});
	}
*/


	function get_zones_result(query_result, cb){
		console.log(get_zones_result);
		var result_zones = [];

		query_result.forEach(function(qr) {
			for(var i = 0; i < qr.identification.zone_id.length; i++){
				if(!result_zones.includes(qr.identification.zone_id[i])){
					result_zones.push(qr.identification.zone_id[i]);
				}
			}

		});
		cb(result_zones);
	}




	function execute_querys(querys, limit, skip, attribute_values, identification, cb){
		console.log(execute_querys);
		//console.log(querys);

		var count = 1;
		var querys_size =  querys.length;

		var result = "";
		var close = "]}";
		var and_open = 0;

		if(querys.length == 1){
			result = querys[0];
			//console.log("11111111111111111111111111111111111", result);
		}else{
			querys.forEach(function(q) {
				//console.log(q);
				//console.log(result); 

				if(and_open == 0){
					//console.log("IIIIIIIIIIIIIIIIIIIIIIIIIII");

					if(result){
						result = '{"or":[' + q + ", " + result + close;
					}else{
						result = '{"or":[' + q + ", ";
						and_open = 1;	
					}
				}else{
					//console.log("EEEEEEEEEEEEEEEEEEEEEEEEEE");
					if(and_open == 1){
						//console.log("IIIIII2222222222222222");
						result = result + q + close;
						and_open = 0;
					}
				}

				console.log("------------------", result);
				count++;
			});
		}
		
		var limit_string = "";
		var skip_string = "";
		var identification_string = "";

		if(typeof limit  !== 'undefined'){
			limit_string = '"limit":"' + String(limit) + '",';
			//console.log(limit_string);
		}

		if(typeof skip  !== 'undefined'){
			skip_string = '"skip":"' + String(skip) + '",';
			//console.log(skip_string);
		}

		//console.log("identification", identification);


		if(typeof identification  !== 'undefined'){
			Object.keys(identification).forEach(function (k) {
				identification_string += '{"identification.'+ k +'":"'+ identification[k] +'"},';
			});

			result = '{"and":['+ identification_string  + result + ']}';
		}

		console.log('{"where":'+ result +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');		

		var final_query = JSON.parse('{"where":'+ result +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');

		attribute_values.find( final_query , function(err, results_query){
			if(err) throw err;

			cb(results_query);

		});

	}

	function build_querys(validations_relational, validations_logical, cb){
		console.log(build_querys);
		var stack = [];

		console.log("RRRRRRRRRRRRRRRRRRRR",validations_relational);
		console.log("LLLLLLLLLLLLLLLLLLLL",validations_logical);

		Array.prototype.push.apply(stack,validations_relational);
		Array.prototype.push.apply(stack,validations_logical);

		stack.sort(function(a, b) {
    		return b.post_order - a.post_order;
		});

		console.log(stack);

		var query = "";
		var close = "";

		query = recursive_build(stack);
		console.log("/////////////////", query);
		cb(query);
	}

/*
	function calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, cb){
		console.log(calculate_expression_V2);

		var stack = [];

		Array.prototype.push.apply(stack,validations_relational);
		Array.prototype.push.apply(stack,validations_logical);

		stack.sort(function(a, b) {
    		return a.post_order - b.post_order;
		});

		//console.log(stack);

		var query = "";
		var close = "";

		var operator_before = 1;

		stack.forEach(function(s) {
			//console.log(s);
			if(s.hasOwnProperty('operator')){
				query = '{"'+ s.operator.toLowerCase() + '":[' + query + "]}";
				operator_before = 2;
			}else{
				if(operator_before == 1){
					if(validations_logical.length == 0){
						query = query + s.search_pattern;
						operator_before = 0;
					}else{
						query = query + s.search_pattern + ",";
						operator_before = 0;
					}
				}else{
					if(operator_before == 2){
						query = query + "," + s.search_pattern;
						operator_before = 1;
					}else{
						query = query + s.search_pattern;
					}
				}
			}
		});

		console.log(query);

		var limit_string = "";
		var skip_string = "";


		if(typeof limit  !== 'undefined'){
			limit_string = '"limit":"' + String(limit) + '",';
			console.log(limit_string);
		}


		if(typeof skip  !== 'undefined'){
			skip_string = '"skip":"' + String(skip) + '",';
			console.log(skip_string);
		}		

		var final_query = JSON.parse('{"where":'+ query +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}')//, "fields":{"identification": true}}')
		//var final_query = JSON.parse('{"where":'+ query +', "limit":"10", "skip":"5", "order":"attribute_group_value_id ASC"}')

		
		console.log('{"where":'+ query +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');
		//console.log(final_query);

		attribute_values.find(final_query , function(err, results_query){
			if(err) throw err;

			cb(results_query);

		});
	}
*/

	function calculate_expression_V2(validations_relational, validations_logical, attribute_values, limit, skip, identification, cb){
		console.log(calculate_expression_V2);

		var stack = [];

		Array.prototype.push.apply(stack,validations_relational);
		Array.prototype.push.apply(stack,validations_logical);

		stack.sort(function(a, b) {
    		return b.post_order - a.post_order;
		});

		//console.log(stack);

		var query = "";
		var close = "";

		var operator_before = 1;

		query = recursive_build(stack);
		//console.log(query);

		var limit_string = "";
		var skip_string = "";
		var identification_string = "";

		if(typeof limit  !== 'undefined'){
			limit_string = '"limit":"' + String(limit) + '",';
			//console.log(limit_string);
		}


		if(typeof skip  !== 'undefined'){
			skip_string = '"skip":"' + String(skip) + '",';
			//console.log(skip_string);
		}

		//console.log("identification", identification, typeof identification);


		if(typeof identification  !== 'undefined'){
			Object.keys(identification).forEach(function (k) {
				console.log("KKKKKKKKKKKKKK", k);
				identification_string +=  '{"identification.'+ k +'":"'+ identification[k] +'"},';
			});

			query = '{"and":['+ identification_string  + query + ']}';
		}

		console.log('{"where":'+ query +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');		

		var final_query = JSON.parse('{"where":'+ query +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}')

		//console.log('{"where":'+ query +','+ limit_string + skip_string+' "order":"attribute_group_value_id ASC"}');

		attribute_values.find(final_query , function(err, results_query){
			if(err) throw err;

			cb(results_query);

		});
	}


	function give_response(response){
		return cb(null, response);
	}


	function recursive_build(stack){
		console.log(recursive_build);
		console.log(stack);

		if(stack.length == 1){
			return stack[0]["search_pattern"];
		}else{
			//console.log(stack.length == 2);
			//console.log(stack[0].hasOwnProperty('operator'));
			if(stack.length == 2 && stack[0].hasOwnProperty('operator')){
				return stack[1]["search_pattern"];
			}else{
				var s = stack[0];
				stack.shift();
				if(s.hasOwnProperty('operator')){
					var q1 = stack.pop()

					if (q1.post_order == s.post_order - 2 && stack[stack.length-1].post_order  != s.post_order - 1){
						return recursive_build(stack) + ', ' + q1["search_pattern"];	
					}else{
						return '{"'+ s.operator.toLowerCase() + '":[' +  recursive_build(stack) + ', ' + q1["search_pattern"] + "]}";
					}
				}
			}
		}
	}


	var add_array = function(detiny_array, add_array, cb){
		var r = Array.prototype.push.apply(detiny_array, add_array);

		cb(r);
	}


	function toHex(str) {
		var hex = '';
		for(var i=0;i<str.length;i++) {
			hex += ''+str.charCodeAt(i).toString(16);
		}
		return hex;
	}



	var query_equal = function (attribute_name_id, value, attribute_values,post_order, operator, cb){
		console.log(query_equal);
		//console.log(operator);

		if(operator == "="){
			var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":"' + value + '"}}'
			//console.log(query, typeof query);
			var obj_query = JSON.parse(query);

			var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":"' + value + '"}'		

			var validations = []

			var obj_resp = {
							//validation_id: res.id,
							//internal_id: res.internal_id,
							//attribute_group_value_id: res.attribute_group_value_id,
							post_order: post_order,
							query: query,
							//search_pattern: JSON.parse('{"values.' + attribute_name_id.toString() + '.value":"' + value + '"}')
							search_pattern: search_pattern
							}

			validations.push(obj_resp);
			cb(validations);
		}else{
			if(operator == ">"){
				var query = '{"where":{"values.teste_hex":{"gt":"' + parseInt(toHex(value)) + '"}}}'
				//console.log(query, typeof query);
				var obj_query = JSON.parse(query);

				var search_pattern = '{"values.teste_hex":{"gt":"' + parseInt(toHex(value)) + '"}}'		

				var validations = []

				var obj_resp = {
								//validation_id: res.id,
								//internal_id: res.internal_id,
								//attribute_group_value_id: res.attribute_group_value_id,
								post_order: post_order,
								query: query,
								//search_pattern: JSON.parse('{"values.' + attribute_name_id.toString() + '.value":"' + value + '"}')
								search_pattern: search_pattern
								}

				validations.push(obj_resp);
				cb(validations);
			}else{
				if(operator == "<"){
					//var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":{"lt":"' + value + '"}}}'
					var query = '{"where":{"values.teste_hex":{"lt":"' + parseInt(toHex(value)) + '"}}}'
					//console.log(query, typeof query);
					var obj_query = JSON.parse(query);

					//var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":{"lt":"' + value + '"}}';
					var search_pattern = '{"values.teste_hex":{"lt":"' + parseInt(toHex(value)) + '"}}';			

					var validations = []

					var obj_resp = {
									//validation_id: res.id,
									//internal_id: res.internal_id,
									//attribute_group_value_id: res.attribute_group_value_id,
									post_order: post_order,
									query: query,
									//search_pattern: JSON.parse('{"values.' + attribute_name_id.toString() + '.value":"' + value + '"}')
									search_pattern: search_pattern
									}

					validations.push(obj_resp);
					cb(validations);
				}else{
					if(operator == "<>"){
						var query = '{"where":{"values.' + attribute_name_id.toString() + '.value":{"neq":"' + value + '"}}}'
						//console.log(query, typeof query);
						var obj_query = JSON.parse(query);

						var search_pattern = '{"values.' + attribute_name_id.toString() + '.value":{"neq":"' + value + '"}}'		

						var validations = []

						var obj_resp = {
										//validation_id: res.id,
										//internal_id: res.internal_id,
										//attribute_group_value_id: res.attribute_group_value_id,
										post_order: post_order,
										query: query,
										//search_pattern: JSON.parse('{"values.' + attribute_name_id.toString() + '.value":"' + value + '"}')
										search_pattern: search_pattern
										}

						validations.push(obj_resp);
						cb(validations);
					}
				}
			}
		}		
	}



	function get_logical(request_rule_id, conditional_logical, operator_logical, cb){
		console.log(get_logical);
		var validations = [];

		conditional_logical.find({where: {rule_id: request_rule_id}} , function(err, results){
			if (err) {
				throw err;
			}

			//console.log(results);
			//console.log(results.length);

			var count = results.length - 1;

			if(results.length > 0){

				results.forEach(function(result) {
					operator_logical.find({where: {external_id: result.operator_logical_id}} , function(err, r){
						if (err) {
							throw err;
						}


						r.forEach(function(op) {
							if(r){
								//console.log(op);

								var operator = op.operator;
								var post_order = result.post_order;

								//console.log(operator);
								//console.log(post_order);
								//console.log("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
								var obj_resp = {
									operator: operator,
									post_order: post_order
								}

								//console.log(obj_resp);

								validations.push(obj_resp);
								if(count == 0){
									//console.log(validations);
									cb(validations);

								}

								count--;
								//cb(validations);

								//console.log(validations);
							}else{
								count--;
							}
						});
					});
					//console.log(validations);
				});
					
			}else{
				cb(validations);
			}
		});
		//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$");
		//console.log(validations);
		//cb(validations);
	}


	function get_relational(request_rule_id, conditions_relational, operator_relational, attribute_values, cb){
		console.log(get_relational);

		conditions_relational.find({where: {rule_id: request_rule_id}} , function(err, results){
			if (err) {
				throw err;
			}

			var validations = [];

			var count = results.length - 1;

			if(results.length > 0){
				results.forEach(function(result) {

					operator_relational.findOne({where: {external_id: result.operator_relational_id}} , function(err, r){
						if (err) {
							throw err;
						}

						if(r){
							var operator = r.operator;
							var attribute_name_id = result.attribute_name_id;
							var value = result.value;
							var post_order = result.post_order;

							//if(operator == "="){
							query_equal(attribute_name_id, value, attribute_values, post_order, operator,  function(vals){
								//console.log("vals", vals);
								//console.log("validations", validations);
								if(operator == ">"){
									//console.log('IIIIIIIIIIIIIFFFFFFFFF111111111111');
									var changed = false;
									for(var i = 0; i < validations.length; i++){
										//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$", validations[i]["search_pattern"]);

										if(validations[i]["search_pattern"].includes('{"values.teste_hex":{"lt":"')){
											console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT1111111111111111111111");

											var inicio = validations[i]["search_pattern"].slice(0, validations[i]["search_pattern"].indexOf('"lt"'));
											var fim = validations[i]["search_pattern"].slice(validations[i]["search_pattern"].indexOf('"lt"'), -2);

											//console.log(inicio);
											//console.log(fim);
											var query = '{"where":' + inicio + '"gt":"' + parseInt(toHex(value)) +'", '+ fim + '}}}';

											console.log("--------", query);

											var obj_query = JSON.parse(query);
											var search_pattern = inicio + '"gt":"' + parseInt(toHex(value)) +'", '+ fim + '}}';

											validations[i]["search_pattern"] = search_pattern;
											validations[i]["query"] = query

											changed = true;

											//cb(validations);						
										}
									}

									if(changed == false){
										Array.prototype.push.apply(validations,vals);
									}

								}else{
									if(operator == "<"){
										//console.log('IIIIIIIIIIIIIFFFFFFFFF2222222222');
										var changed = false;
										for(var i = 0; i < validations.length; i++){
											//console.log("####################################", validations[i]["search_pattern"]);
											if(validations[i]["search_pattern"].includes('{"values.teste_hex":{"gt":"')){
												console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT1111111111111111111111222222222222222222222222");
												var query = '{"where":' + validations[i]["search_pattern"].slice(0, -2) + ', "lt":"' + parseInt(toHex(value)) + '"}}}';
												console.log("%%%%%%%%%%%%%%%%",query);
												var obj_query = JSON.parse(query);
												var search_pattern = validations[i]["search_pattern"].slice(0, -2) + ', "lt":"' + parseInt(toHex(value)) + '"}}';


												validations[i]["search_pattern"] = search_pattern;
												validations[i]["query"] = query

												changed = true;

												//cb(validations);						
											}
										}

										if(changed == false){
											Array.prototype.push.apply(validations,vals);
										}
									}
									else{
										//									console.log('EEEEEEEEEEEEEEEEEEEEEEEEe');
										Array.prototype.push.apply(validations,vals);
									}

								}
								//Array.prototype.push.apply(validations,vals);
								//console.log("count--", count);
								if(count == 0){
									//console.log("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
									cb(validations);
								} 
								count--;
							});
						}
					});
				});
			}else{
				cb(validations);
			}
		});
		
	}


	function get_rule_info(request_rule_id, rules, cb){
		console.log(get_rule_info);

		rules.find({where: {external_id: request_rule_id}} , function(err, results){
			if (err) {
				throw err;
			}

			console.log("results", results);

			var validations = [];

			if(results.length > 0){
				validations.push(results.att_g_val_identification_validation);
				console.log("results.att_g_val_identification_validation", results.att_g_val_identification_validation)
				cb(validations);
			}else{
				cb(validations);
			}
		});
	}






	Ektixvalidationrequest.remoteMethod('rule_validation', {
		description:"Método para validar regras",
		http:{
			path:'/rule_validation',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_rule_id', type : 'number', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false}
			//{ arg : 'request_template_id', type : 'number', required: false},
			//{ arg : 'request_product_id', type : 'number', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

/*
	Ektixvalidationrequest.remoteMethod('multiRule_validation', {
		description:"Método para validar várias regras em simultâneo",
		http:{
			path:'/multiRule_validation',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_array_rules_id', type : 'array', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false}
			//{ arg : 'request_template_id', type : 'number', required: false},
			//{ arg : 'request_product_id', type : 'number', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});
*/

	Ektixvalidationrequest.remoteMethod('multiRule_validation', {
		description:"Método para validar várias regras em simultâneo",
		http:{
			path:'/multiRule_validation',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'request_array_rules_id', type : 'array', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false},
			{ arg : 'identification', type : 'object', required: false}

			//{ arg : 'request_template_id', type : 'number', required: false},
			//{ arg : 'request_product_id', type : 'number', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

/*
	Ektixvalidationrequest.remoteMethod('get_zones', {
		description:"Método para obter zonas a partir do product_id",
		http:{
			path:'/get_zones',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'product_id', type : 'string', required: true},
			{ arg : 'limit', type : 'number', required: false},
			{ arg : 'skip', type : 'number', required: false}
			//{ arg : 'request_template_id', type : 'number', required: false},
			//{ arg : 'request_product_id', type : 'number', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});
*/


};