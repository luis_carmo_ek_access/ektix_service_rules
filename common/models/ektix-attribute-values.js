'use strict';

module.exports = function(Ektixattributevalues) {

	Ektixattributevalues.delete_AttributeValues = function(seats_values, identification, field, cb) {

		var app = Ektixattributevalues.app;

		if (seats_values.length > 0 && field == 'seat') {

			seats_values.forEach(function(value) {

				Ektixattributevalues.find({where: {'identification.seat_id': value.seat_id}}, function(err, results) {

					results.forEach(function(result) {

						Ektixattributevalues.destroyById(result.id, function(err, result_delete) {

							if (err) {
								throw err;
							}

						});

					});		

				});

			});

		} else {

			if (field == 'venue' || field == 'template' || field == 'zone' || field == 'block') {

				Ektixattributevalues.find({where: identification}, function(err, results) {

					results.forEach(function(result) {

						Ektixattributevalues.destroyById(result.id, function(err, result_delete) {

							if (err) {
								throw err;
							}

						});

					});

					return cb(null, {success: true});

				});

			} else if (field == 'seat') {

				var identification_string = "";

				Object.keys(identification).forEach(function (k) {
					identification_string += '{"'+ k +'":'+ identification[k] +'}';
				});

				if (identification_string != "") {

					Ektixattributevalues.find({where: JSON.parse(identification_string)}, function(err, results) {

						results.forEach(function(result) {

							Ektixattributevalues.destroyById(result.id, function(err, result_delete) {

								if (err) {
									throw err;
								}

							});

						});

						return cb(null, {success: true});

					});

				} else {

					return cb(null, {success: false});

				}

			} else {

				return cb(null, {success: false});

			}

		}

	}

	Ektixattributevalues.update_Attributes = function(identification, values, field, cb) {

		var app = Ektixattributevalues.app;

		// { "$and" : [ {  }, {  } ] }

		if (identification != null && values.length > 0) {

			if (field == 'seat') {

				var identification_string = "";

				Object.keys(identification).forEach(function (k) {
					identification_string += '{"'+ k +'":'+ identification[k] +'}';
				});

				identification = JSON.parse(identification_string);

			}
			
			Ektixattributevalues.find({where: identification}, function(err, result) {

				if (err) {

					throw err;

				}

				if (result != null) {

					values.forEach(function(up) {

						var key = Object.keys(up)[0];
						var value = up[key];

						result[0]['values'][key] = value;

						Ektixattributevalues.updateAll(identification, {values: result[0]['values']}, function(err, results) {

							if (err){

								throw err;
								return cb(null, {return: false, identification: identification, values: result[0]['values'], message: "Internal Error"});

							}
			
						});

					});

					return cb(null, {succes: true, message: 'Record(s) Updated!'});

				} else {

					return cb(null, {succes: false, message: 'No Results Found!'});

				}

			});

		} else {

			return cb(null, {success: false, message: 'One of the following (Identification / Values) is Empty!'});

		}

	}

	Ektixattributevalues.insert_AttributeValues = function(attribute_group_value_id, values, cb) {

		var app = Ektixattributevalues.app;
		var ektix_attribute_group_values = app.models.ektix_attribute_group_values;

		ektix_attribute_group_values.findOne({where: {external_id: attribute_group_value_id}}, function(err, results) {
			if (err){
				throw err;
				return cb(null, {inserido: false, attribute_group_value_id: attribute_group_value_id});
			}

			if(results != null){
				//console.log("IF");
				var obj_insert = {
					attribute_group_value_id: attribute_group_value_id,
					values: values,
					internal_id: results.id
				}
				Ektixattributevalues.create(obj_insert, function(err, results) {
					if (err)throw err

					//console.log(results);
					return cb(null, {inserted: true, attribute_group_value_id: attribute_group_value_id});
					

				});
			}else{
				return cb(null, {inserted: false, attribute_group_value_id: attribute_group_value_id});
			}
		});
	}


	Ektixattributevalues.update_AttributeValues = function(attribute_group_value_id, values, action, field, cb) {

		var app = Ektixattributevalues.app;
		var ektix_attribute_group_values = app.models.ektix_attribute_group_values;

		Ektixattributevalues.findOne({where: {attribute_group_value_id: attribute_group_value_id}}, function(err, results) {
			
			if (err){
				throw err;
				return cb(null, {return: false, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa do group value"});
			}

			if(results != null){
				//console.log(results);
				//console.log(attribute_group_value_id);
				//console.log(results.id);

				Ektixattributevalues.findOne({where: {attribute_group_value_id: attribute_group_value_id}}, function(err, results) {
					if (err){
						throw err;
						return cb(null, {return: false, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa Ektixattributevalues"});
					}

					//console.log(results);

					if(results){
						if(action == "d"){

							values.forEach(function(up) {
									//console.log(up);
									delete results['values'][up];									
								});

								//console.log(results);

								Ektixattributevalues.updateAll({attribute_group_value_id: attribute_group_value_id}, {values: results['values']}, function(err, results) {
									
									if (err){
										throw err;
										return cb(null, {return: false, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa Ektixattributevalues"});
									}
									
									//return cb(null, {return: true, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa"});
								});

							return cb(null, {return: true, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa"});
						}else{

							if(action == "u"){

								values.forEach(function(up) {
									console.log(up);
									var key = Object.keys(up)[0];
									results['values'][key] =  up[key];									

								});

								//console.log(results);

								Ektixattributevalues.updateAll({attribute_group_value_id: attribute_group_value_id}, {values: results['values']}, function(err, results) {
									if (err){
										throw err;
										return cb(null, {return: false, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa Ektixattributevalues"});
									}
									
									return cb(null, {return: true, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "erro na pesquisa"});
								});
							}

						}
					}else{
						return cb(null, {return: false, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "attribute group value não existe"});
					}




					//console.log(results);
					return cb(null, {inserted: true, attribute_group_value_id: attribute_group_value_id});
					

				});
			}else{
				return cb(null, {return: false, attribute_group_value_id: attribute_group_value_id, values:values, action: action, msg: "attribute group value não existe"});
			}
		});
	}


	Ektixattributevalues.correct_AttributeValues = function(cb) {
		var app = Ektixattributevalues.app;
		var response_model = app.models.EktixValidationResponse;
		var conditional_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var operator_logical = app.models.EktixOperatorLogical;
		var operator_relational = app.models.EktixOperatorRelational;
		var rules = app.models.EktixRules;
		var validation_response = app.models.EktixValidationResponse;

		var zone_values_temp_two = ["1358", "1359", "1360"];

		Ektixattributevalues.find({where: {"identification.template_id": "2"}}, function(err, results_template_two) {
			if(err) throw err;

			results_template_two.forEach(function(rtt) {
				var result_zone = [];

				for(var i = 0; i < zone_values_temp_two.length; i++){
					for(var j = 0; j < rtt["identification"]["zone_id"].length; j++){
						if(zone_values_temp_two[i] == rtt["identification"]["zone_id"][j]){					
							result_zone.push(zone_values_temp_two[i]);
						}
					}
				}

				rtt["identification"]["zone_id"] = result_zone;

				var delete_id = rtt.id;

				var insert = {
					attribute_group_value_id: rtt.attribute_group_value_id,
					internal_id: rtt.internal_id,
					values: rtt.values,
					identification: rtt.identification
				};

				Ektixattributevalues.destroyById(delete_id, function(err, destroy_id) {
					if(err) throw err;

					console.log("Destroy");
					console.log("$$$$$$$$$$", delete_id);

					Ektixattributevalues.create(insert, function(err, insert_obj){
						if(err) throw err;

						console.log("CREATE");
						console.log("--------------", insert_obj.id);
					});
				});
			});
		});

		Ektixattributevalues.find({where: {"identification.template_id": "1"}}, function(err, results_template_one) {
			if(err) throw err;

			results_template_one.forEach(function(rto) {
				var result_zone = [];

				for(var j = 0; j < rto["identification"]["zone_id"].length; j++){
					if(!zone_values_temp_two.includes(rto["identification"]["zone_id"][j])){
						if(!result_zone.includes(rto["identification"]["zone_id"][j])){						
							result_zone.push(rto["identification"]["zone_id"][j]);
						}
					}
				}

				rto["identification"]["zone_id"] = result_zone;

				var delete_id = rto.id;

				var insert = {
					attribute_group_value_id: rto.attribute_group_value_id,
					internal_id: rto.internal_id,
					values: rto.values,
					identification: rto.identification
				};

				Ektixattributevalues.destroyById(delete_id, function(err, destroy_id) {
					if(err) throw err;

					console.log("Destroy");
					console.log("$$$$$$$$$$", delete_id);

					Ektixattributevalues.create(insert, function(err, insert_obj){
						if(err) throw err;

						console.log("CREATE");
						console.log("--------------", insert_obj.id);
					});
				});
			});
		});
		return cb(null, true);
	}


	Ektixattributevalues.get_values = function(attribute_group_name_id, identification, cb) {
		var app = Ektixattributevalues.app;
		var ektix_attribute_names = app.models.EktixAttributeNames;

		ektix_attribute_names.find({where: {attribute_group_name_id: attribute_group_name_id}, fields: {external_id: true, tag: true, attribute_group_name_id: true} } , function(err, results){
			if (err) throw err;

			console.log(results);
			var identification_string = "";
			var test_values = "";
			var query = "";

			var count = 1
			if(results.length > 0){
				var test_values = "";
				results.forEach(function(q) {
					console.log(q);
					if(count == results.length){
						test_values += '{"values.'+ String(q.external_id) + '":{"exists":true}}'
					}else{
						test_values += '{"values.'+ String(q.external_id) + '":{"exists":true}},'
					}
					count++;
					console.log(test_values);
				});

				query = '{"or":['+ test_values + ']}';
			}

			if(typeof identification  !== 'undefined'){
				Object.keys(identification).forEach(function (k) {
					identification_string += '{"identification.'+ k +'":"'+ identification[k] +'"},';
				});
				
				query = '{"and":['+ identification_string  + query + ']}';
			}



			console.log('{"where":'+ query +', "order":"attribute_group_value_id ASC"}');

			var final_query = JSON.parse('{"where":'+ query +', "order":"attribute_group_value_id ASC"}');

			Ektixattributevalues.find( final_query , function(err, results_query){
				if(err) throw err;

				var send_response = {
					"tags": results,
					"values": results_query}

				return cb(null, send_response);
			});
		});
	}


	Ektixattributevalues.set_values = function(attribute_group_name_id, operation, identification, values, cb) {

	}

	Ektixattributevalues.remoteMethod('delete_AttributeValues', {
		description: 'Remover Atributos -> Field(venue, template, zone, block, seat)',
		http: {
			path: '/delete_AttributeValues',
			verb: 'post',
			status: 200,
			errorStatus: 400
		},
		accepts: [
			{ arg: 'values', type: 'array'},
			{ arg : 'identification', type : 'object', required: false},
			{ arg : 'field', type : 'string', required: true},
		],
		returns: [
			{ arg: 'array', root: true },
		]
	});

	Ektixattributevalues.remoteMethod('update_Attributes', {

		'description': 'Update Attributes ( Field(venue, template, zone, block, seat) )',
		'http': {
			path: '/update_Attributes',
			verb: 'post',
			status: 200,
			errorStatus: 400
		},
		accepts: [
			{ arg: 'identification', type: 'object', required: true },
			{ arg: 'values', type: 'array', required: true},
			{ arg : 'field', type : 'string', required: true},
		],
		returns: [
			{ arg: 'array', root: true},
		]

	});

	Ektixattributevalues.remoteMethod('insert_AttributeValues', {
		description:"Inserir atributos com values",
		http:{
			path:'/insert_AttributeValues',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'attribute_group_value_id', type : 'number', required: true},
			{ arg : 'values', type : 'object'},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixattributevalues.remoteMethod('update_AttributeValues', {
		description:"Update, delete de values id, values e action(u,d)",
		http:{
			path:'/update_AttributeValues',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'attribute_group_value_id', type : 'number', required: true},
			{ arg : 'values', type : 'array', required: true},
			{ arg : 'action', type : 'string', required: true},
			{ arg : 'field', type : 'string', required: false},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixattributevalues.remoteMethod('correct_AttributeValues', {
		description:"correct db values",
		http:{
			path:'/correct_AttributeValues',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			//{ arg : 'attribute_group_value_id', type : 'number', required: true},
			//{ arg : 'values', type : 'array', required: true},
			//{ arg : 'action', type : 'string', required: true},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixattributevalues.remoteMethod('get_values', {
		description:"get values dado attribute_group_name_id e identification",
		http:{
			path:'/get_values',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'attribute_group_name_id', type : 'number', required: true},
			{ arg : 'identification', type : 'object', required: false}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixattributevalues.remoteMethod('set_values', {
		description:"set values dado attribute_group_name_id, operation(A - add, D - delete, C - change), identification(opcional) e values(opcional)",
		http:{
			path:'/set_values',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'attribute_group_name_id', type : 'number', required: true},
			{ arg : 'operation', type : 'string', required: true},
			{ arg : 'identification', type : 'object', required: false},
			{ arg : 'values', type : 'object', required: false}

		],
		returns:[
			{arg : 'array', root : true},
		],
	});


};
