'use strict';

module.exports = function(Ektixattributegroupnames) {
	var Moment = require('moment-timezone');

	Ektixattributegroupnames.get_byExtID = function(external_id, cb) {

		Ektixattributegroupnames.findOne({where: {external_id: external_id}}, function(err, results) {
			if (err){ 
				return cb(null, []);
				throw err;
			}

			if(results == null){
				return cb(null, []);
			}else{
				var response = {
					"external_id": results.external_id,
					"code": results.code
				}
				give_response(response, cb)
			}

		});
	}

	Ektixattributegroupnames.post_groupNames = function(gName, cb) {
		var date = Moment().utc("Europe/Lisbon").format();

		Ektixattributegroupnames.find({where: {external_id: gName.external_id, code: gName.code}} , function(err, results){
			if (err) {
				throw err;
			}

			if (results.length > 0){
				var response = {
						"inserted": false,
						"reason": "já existe na base de dados",
						"code": gName.code,
						"external_id": gName.external_id
					}
				give_response(response, cb);
			}else{
				gName.created_date = date;
				gName.modified_date = date;
				
				Ektixattributegroupnames.create(gName, function(err, results) {
					if (err){ 
						var response = {
							"inserted": false,
							"reason": "erro na criação",
							"code": gName.code,
							"external_id": gName.external_id
						}
						throw err;
						give_response(response, cb);
					}

					var response = {
							"inserted": true,
							"code": gName.code,
							"external_id": gName.external_id
						}

					give_response(response, cb);
				});
			}
		}); 		

	}

	function give_response(response, cb){
		return cb(null, response);

	}


	Ektixattributegroupnames.remoteMethod('get_byExtID', {
		description:"",
		http:{
			path:'/get_byExtID',
			verb : 'get',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'external_id', type : 'number', required: true},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixattributegroupnames.remoteMethod('post_groupNames', {
		description:"",
		http:{
			path:'/post_groupNames',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'gName', type : 'object', required: true},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});
};
