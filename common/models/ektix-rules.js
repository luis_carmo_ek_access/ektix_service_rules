'use strict';

module.exports = function(Ektixrules) {
	/*Ektixrules.observe('after save', function(ctx, next) {
		var app = Ektixrules.app;
		//console.log(app.models);
		//var rules = app.models.EktixRules;
		var validation_request = app.models.EktixValidationRequest;


		if (ctx.instance) {
			console.log('Saved %s#%s', ctx.Model.modelName, ctx.instance.id);

			var rule_id = ctx.instance.external_id;
			var request_array_rules_id = [rule_id];
			var limit = 1;
			var skip = 0;
			var identification = {"template_id" : String(ctx.instance.att_g_val_identification_validation.template_id)};

			var distinct_tag = "identification.zone_id";


			validation_request.distinctRule_validation(request_array_rules_id, identification, distinct_tag,  function(err, dr_val) {

				dr_val.forEach(function(zone) {

				validation_request.multiRule_bool(request_array_rules_id, limit, skip, identification, function(err, mr_bool) {
	      			if (err) throw err;

	      			console.log("QQQQQQQQQQQQ");

      			});

			});






		} else {
			console.log('Updated %s matching %j',
		  	ctx.Model.pluralModelName,
		  	ctx.where);
		}
		next();
	});



*/










	Ektixrules.delete_rules = function(rules_id, cb) {
		var _ = require('underscore');

		var app = Ektixrules.app;
		var conditions_logical = app.models.EktixConditionsLogical;
		var conditions_relational = app.models.EktixConditionsRelational;
		var rules = app.models.EktixRules;

		var list_missed = [];
		
		console.log(rules_id);

		rules_id.forEach(function(rule_id) {
			del_rule(rule_id, rules,  function(deleted_rule){
				if(deleted_rule.length > 0){
					if (_.findWhere(list_missed, rule_id) == null) {
	    				list_missed.push(rule_id);
					}
				}

				delete_condition_logical(rule_id, conditions_logical, function(deleted_condition_logical){
					if(deleted_condition_logical.length > 0){
						if (_.findWhere(list_missed, rule_id) == null) {
		    				list_missed.push(rule_id);
						}
					}

					delete_condition_relational(rule_id, conditions_relational, function(deleted_condition_relational){
						if(deleted_condition_relational.length > 0){
							if (_.findWhere(list_missed, rule_id) == null) {
			    				list_missed.push(rule_id);
							}
						}

						//if(list_missed.length > 0){
							//return cb(null, list_missed);
						//}else{
							//return cb(null, true);
						//}							
					});
				});
			});
		});

		if(list_missed.length > 0){
			return cb(null, list_missed);
		}else{
			return cb(null, true);
		}	
	}


	function del_rule(rule_id, rules, cb){
		console.log("del_rule");
		console.log(rule_id);
		var deleted = false;

		rules.find({where: {external_id: rule_id}}, function(err, result){
				if (err) {
					throw err;
				}
				
				console.log("--------------", result);

				if(result.length > 0){
					result.forEach(function(r) {				
						rules.destroyById(r.id , function(err, result){							
							if (err) {
								throw err;
							}

							console.log("DESTROY", result);
						});
					});

					rules.find({where: {external_id: rule_id}}, function(err, result){
						if (err) {
							throw err;
						}

						if(result.length == 0){
							console.log("IIIIIIIIFFFFFFFFFFFFFFFFF1");
							cb(result);	
						}else{
							console.log("EEEEEEEEEEEELLLLLLLLLLLLSSSSSSSSSSSEEEEEEEEEEE2");
							cb(result);	
						}
					});						
				}else{
					console.log("EEEEEEEEEEEELLLLLLLLLLLLSSSSSSSSSSSEEEEEEEEEEE2");
					cb(result);
				}
			});
	}


	function delete_condition_logical(rule_id, conditions_logical, cb){
		console.log("delete_condition_logical");
		console.log(rule_id);
		var deleted = false;

		conditions_logical.find({where: {rule_id: rule_id}}, function(err, result){
				if (err) {
					throw err;
				}
				
				console.log("--------------", result);

				if(result.length > 0){

					result.forEach(function(r) {				
						conditions_logical.destroyById(r.id , function(err, result){							
							if (err) {
								throw err;
							}
						});
					});

					conditions_logical.find({where: {rule_id: rule_id}}, function(err, result){
						if (err) {
							throw err;
						}

						if(result.length == 0){
							cb(result);		
						}else{
							cb(result);		
						}
					});	
				}else{
					cb(result);
				}
			});
	}


	function delete_condition_relational(rule_id, conditions_relational, cb){
		console.log("delete_condition_relational");
		console.log(rule_id);
		var deleted = false;

		conditions_relational.find({where: {rule_id: rule_id}}, function(err, result){
				if (err) {
					throw err;
				}
				
				console.log("--------------", result);

				if(result.length > 0){

					result.forEach(function(r) {				
						conditions_relational.destroyById(r.id , function(err, result){							
							if (err) {
								throw err;
							}
						});
					});

					conditions_relational.find({where: {rule_id: rule_id}}, function(err, result){
						if (err) {
							throw err;
						}

						if(result.length == 0){
							cb(result);		
						}else{
							deleted = false;
							cb(result);		
						}
					});	
				}else{
					cb(result);
				}
			});
	}


	Ektixrules.remoteMethod('delete_rules', {
		description:"Método para apagar a regra(rules, conditional e logical)",
		http:{
			path:'/delete_rules',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'rules_id', type: 'array', required: true}
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

};
