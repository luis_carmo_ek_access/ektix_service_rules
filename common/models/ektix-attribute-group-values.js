'use strict';

module.exports = function(Ektixattributegroupvalues) {
	var Moment = require('moment-timezone');

	Ektixattributegroupvalues.get_byExtID = function(external_id, cb) {

	Ektixattributegroupvalues.findOne({where: {external_id: external_id}}, function(err, results) {
			if (err){ 
				return cb(null, []);
				throw err;
			}

			if(results == null){
				return cb(null, []);
			}else{
				var response = {
					"external_id": results.external_id
				}

				give_response(response, cb);
			}
		});
	}


	Ektixattributegroupvalues.post_groupValues = function(external_id, cb) {
		var date = Moment().utc("Europe/Lisbon").format();

		Ektixattributegroupvalues.find({where: {external_id: external_id}} , function(err, results){
			if (err) {
				throw err;
			}

			if (results.length > 0){
				var response = {
					"inserted": false,
					"reason": "já existe na base de dados",
					"external_id": external_id
				}
				give_response(response, cb);
			}else{
				var obj_insert = {
					"external_id": external_id,
					"created_date": date,
					"modified_date": date
				}

				Ektixattributegroupvalues.create(obj_insert, function(err, results) {
					if (err){ 
						var response = {
							"inserted": false,
							"reason": "erro na criação",
							"external_id": external_id
						}
						throw err;
						give_response(response, cb);
					}

					var response = {
							"inserted": true,
							"external_id": external_id
						}

					give_response(response, cb);
				});
			}
		});
	}


	function give_response(response, cb){
		return cb(null, response);

	}



	Ektixattributegroupvalues.remoteMethod('get_byExtID', {
		description:"",
		http:{
			path:'/get_byExtID',
			verb : 'get',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'external_id', type : 'number', required: true},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});


	Ektixattributegroupvalues.remoteMethod('post_groupValues', {
		description:"",
		http:{
			path:'/post_groupValues',
			verb : 'post',
			status: 200,
			errorStatus: 400
		},
		accepts : [
			{ arg : 'external_id', type : 'number', required: true},
		],
		returns:[
			{arg : 'array', root : true},
		],
	});

};
