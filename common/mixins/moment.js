 module.exports = function(Model, options) {
 	var Moment = require('moment-timezone');
 	date = Moment().utc("Europe/Lisbon").format()

  // Model is the model class
  // options is an object containing the config properties from model definition
  Model.defineProperty('created', {type: Date, default: date});
  Model.defineProperty('modified', {type: Date, default: date});
}