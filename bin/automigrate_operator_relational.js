// Copyright IBM Corp. 2015,2016. All Rights Reserved.
// Node module: loopback-example-database
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

var path = require('path');
var Moment = require('moment-timezone');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.ektix_rules_mongodb;

// http://www.deloitte-guiafiscal.com/iva/taxas-de-iva

ds.automigrate('ektix_operator_relational', function(err) {
	if (err) throw err;

	date = Moment().utc("Europe/Lisbon").format();

	var opers = [
	{
		"external_id": 1,
		"operator": "=",
		"precedence": 1,
		"created_date": date,
		"modified_date": date
	},
	{
		"external_id": 2,
		"operator": ">",
		"precedence": 1,
		"created_date": date,
		"modified_date": date
	},
	{
		"external_id": 3,
		"operator": "<",
		"precedence": 1,
		"created_date": date,
		"modified_date": date
	},
	{
		"external_id": 4,
		"operator": "<>",
		"precedence": 1,
		"created_date": date,
		"modified_date": date
	}]

	var count = opers.length;
	opers.forEach(function(op) {
    app.models.ektix_operator_relational.create(op, function(err, model) {
      if (err) throw err;

      console.log('Created:', model);

      count--;
      if (count === 0)
        ds.disconnect();
    });
  });


}); 
