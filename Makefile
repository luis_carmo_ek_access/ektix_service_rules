 all: load_op_relational load_op_logical load_rule_type

load_op_relational:
	node bin/automigrate_operator_relational.js 

load_op_logical:
	node bin/automigrate_operator_logical.js 

load_rule_type:
	node bin/automigrate_rule_type.js 
